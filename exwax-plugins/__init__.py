# -*- coding: utf-8 -*-
import mutagen, re, os, cPickle as pickle, sys, traceback
import requests, lxml
import urllib2

import json
from bs4 import BeautifulSoup
from pathos.multiprocessing import ProcessingPool
import concurrent.futures
from multiprocessing import Pool
from io import BytesIO
import time

from shutil import copyfile
import subprocess
import errno
#import sys,re
#import pyexiv2
import PIL.Image

import gi
gi.require_version('GExiv2', '0.10')
from gi.repository.GExiv2 import Metadata

from PyQt4.QtCore import *
from PyQt4.QtGui import *
import FlowLayout

from puddlestuff.plugins import status, add_shortcuts, connect_control

#sys.setrecursionlimit(2)

flags = os.O_CREAT | os.O_EXCL | os.O_WRONLY

obj = QObject()

formats = ['.jpg','.Jpg','.JPG','.png','.Png','.PNG','.bmp','.Bmp','.BMP']

show_notifications = False

def clearLayout(self, layout):
    if layout is not None:
        while layout.count():
            item = layout.takeAt(0)
            widget = item.widget()
            if widget is not None:
                widget.deleteLater()
            else:
                self.clearLayout(item.layout())

class ImageInfo(QWidget):
    """docstring for ImageInfo"""
    resultPath = ""
    def __init__(self, parent, arg , original=None, dims=None):
        super(ImageInfo, self).__init__(parent)
        self.image = QImage()
        self.parentDiag = parent
        self.label_dims = QLabel()
        self.label_file = QLabel()
        self.label_image = QLabel()
        self.label_image.setFixedSize(250,250)
        self.layout = QVBoxLayout()
        self.layout.addWidget(self.label_file)
        self.layout.addWidget(self.label_image)
        self.layout.addWidget(self.label_dims)
        self.filePath = arg
        self.folderPath = self.parent().folderPath
        self.qimage = QImage()
        self.webImage = False
        self.originalUrl = original
        if not arg.startswith('/'):
            self.qimage.loadFromData(arg)
            self.label_file.setText(self.originalUrl.split('/')[-1])
            self.label_dims.setText(dims)
            self.webImage = True
        else:
            self.qimage = QImage(arg)
            self.label_file.setText(os.path.basename(arg))
            self.label_dims.setText(str(self.qimage.width()) + ", " + str(self.qimage.height()))
        self.image = QPixmap(self.qimage).scaled(250,250)
        self.label_image.setPixmap(self.image)
        self.setStyleSheet("background-color: grey; margin:5px; border:1px solid black; ")
        self.setLayout(self.layout)

    def mousePressEvent(self,event):
        print "clicked!: "#+self.filePath
        if self.webImage:
            filePath = os.path.join(self.folderPath, "cover.jpg")
            self.origImage = QImage()
            self.origImage.loadFromData(requests.get(self.originalUrl).content)
            self.origImage.save(filePath, 'jpg')
            self.parentDiag.returnedPath = filePath
        else:
            self.parentDiag.returnedPath = self.filePath
        self.parentDiag.accept()

def getDims(filePath):
    print "getting dims for: "+filePath
    metadata = Metadata(filePath)
    return (metadata.get_pixel_height(), metadata.get_pixel_width())

class GoogleImage(object):
    def __init__(self, linkThumbnail, linkOriginal, dimsOriginal):
        super(GoogleImage, self).__init__()
        self.linkThumbnail = linkThumbnail
        self.linkOriginal = linkOriginal
        self.dimsOriginal = dimsOriginal
        self.imgRaw = None
    def __str__(self):
        return 'Thumb: ' + str(self.linkThumbnail)  + '\nOrig: ' + str(self.linkOriginal) + '\ndims: ' + str(self.dimsOriginal)

class Release(object):
    def __init__(self, samplefile):
        super(Release, self).__init__()
        self.sampleFile = samplefile

class FolderArtDialog(QDialog):
    """docstring for FolderArtDialog"""
    images = []
    returnedPath = ""
    def __init__(self):
        super(FolderArtDialog, self).__init__()
        self.boxLayout = QVBoxLayout()
        #self.layout = QGridLayout()
        self.searchButton = QPushButton("Search Google")
        self.searchButton.clicked.connect(self.getImagesFromGoogle)
        self.resize(900,700)
        self.layWidget = QWidget()
        self.scroll = QScrollArea()
        self.scroll.setWidget(self.layWidget)
        self.scroll.setWidgetResizable(True)
        self.scroll.setFixedHeight(400)
        self.flowLay = FlowLayout.FlowLayout(self.layWidget)
        self.boxLayout.addWidget(self.scroll)
        self.boxLayout.addWidget(self.searchButton)
        self.setLayout(self.boxLayout)
        
        #self.show()
   # def clearLayout(self):
   #     self.

    def addImageInfo(self, _file, googleImage=False):
        if googleImage == True:
            if 'lookaside.fb' in _file.linkOriginal:
                return
            imgInfo = ImageInfo(self, _file.imageRaw, _file.linkOriginal, _file.dimsOriginal)
        else:
            imgInfo = ImageInfo(self, _file)
        print "added image..."
        self.images.append(imgInfo)
        self.flowLay.addWidget(imgInfo)

    def findImages(self,path):
        self.folderPath = path
        del self.images[:]
        #clearLayout(self,self.layout)
        dirlist = os.listdir(path)

        count = 0
        #self.layout.addWidget(self.searchButton)
        for f in dirlist:
            #if f.endswith('.jpg') or f.endswith('.png'):
            front, ext = os.path.splitext(f)
            if ext in formats:
                self.addImageInfo(path +"/"+ f)
                count = count+1
        if count == 0:
            self.flowLay.addWidget(QLabel("No Images found in "+path+" !"))
            #with open('~/missing_folderart', 'a') as file_obj:
            #    file_obj.write(path + '\n')
        self.setLayout(self.boxLayout)
        if count == 0:
            return False
        else:
            return True

    def getImagesFromGoogle(self):
        googleImages = []
        clearLayout(self, self.flowLay)
        header={'User-Agent':"Mozilla/5.0"}
        header2={'User-Agent':"Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.134 Safari/537.36"}
        
        query = os.path.basename(self.folderPath)
        query = query.replace(" ", "+")
        
        url = "https://www.google.co.in/search?q=" + query + "&source=lnms&tbm=isch"
        soupLowRes = BeautifulSoup(urllib2.urlopen(urllib2.Request(url, headers=header )), "html.parser", from_encoding='utf-8')
        soupHiRes = BeautifulSoup(urllib2.urlopen(urllib2.Request(url, headers=header2 )), "html.parser", from_encoding='utf-8')
        fullsizeImagesJson = soupHiRes.find_all("div",{"class":"rg_meta"})
        dims = [a.text for a in soupHiRes.find_all("div", {"class": re.compile("rg_ilmbg")})[:8]]
        
        print(dims)
        niceDims = []
        for d in dims:
            split = d.split(u'\xa0')
            niceDims.append(split[0] + ' x ' + split[-1])
        #print('DIMS :'+str(len(dims)) +'\n' + str(niceDims))
        thumbs = [a['src'] for a in soupLowRes.find_all("img", {"src": re.compile("gstatic.com")})[:8]]
        print('thumbs: '+ str(thumbs))
        fullsizeImagesLinks = [json.loads(entry.text)["ou"] for entry in fullsizeImagesJson[:8]]
        start = time.time()
        for thumb, orig, dim in zip(thumbs, fullsizeImagesLinks, niceDims):
            googleImages.append(GoogleImage(thumb, orig, dim))
            #print str(googleImage)
        #p = ProcessingPool(processes=8)
        #p = Pool(processes=25)
        #p.map(self.processJson,  fullsizeImagesLinks)
        #p.terminate()
        #p.join()
        #for link in tuples:
        #    tupleRes = self.processJson(link)
        #    self.addImageInfo(tupleRes[0], tupleRes[1])
        with concurrent.futures.ThreadPoolExecutor(max_workers=4) as executor:
            # Start the load operations and mark each future with its URL
            #executor.map(self.processJson, tuples)
            future_to_url = {executor.submit(self.processJson, imageJson, 30): imageJson for imageJson in googleImages}
            for future in concurrent.futures.as_completed(future_to_url):
                url = future_to_url[future]
                try:
                    gImage = future.result()
                except Exception as exc:
                    print('%r generated an exception: %s' % (url, exc))
                else:
                    self.addImageInfo(gImage, googleImage=True)
        self.setLayout(self.boxLayout)
        print('took: ' + str(time.time() - start))

    def processJson(self, googleImage, arg=None):
        #link = json.loads(jsonText.text)["ou"]
        googleImage.imageRaw = requests.get(googleImage.linkThumbnail).content
        return googleImage 
        #self.addImageInfo(imageRaw)



def highlight_dupe_field():
    #field, ok = QInputDialog.getText(None, 'puddletag', 'Field to compare')

    files = status['selectedfiles']
    if not files or len(files) < 1:
        return

    highlight = []

    for f in files:
        filePath = f.get("__dirpath")
        if (os.path.isfile(filePath + "/folder.jpg")):
            metadata = pyexiv2.ImageMetadata(filePath + "/folder.jpg")
            metadata.read()
            print metadata.dimensions
            if metadata.dimensions != (250,250):
                highlight.append(f)
        else:
            highlight.append(f)

        print filePath

    obj.emit(SIGNAL('highlight'), highlight)
    obj.sender().setChecked(True)

def remove_highlight():
    obj.emit(SIGNAL('highlight'), [])
    obj.sender().setChecked(False)

def filePaths2FolderPaths(files):
    folders = []
    for f in files:
        print "file: "+f.dirpath
        folderPath = f.dirpath
        if not folderPath in folders:
            print "adding "+folderPath
            folders.append(folderPath)
    return folders


class MoveFolderDialog(QDialog):
    """Dialog to confirm the moving of folders to the local music folder structure"""
    def __init__(self, releases):
        super(MoveFolderDialog, self).__init__()
        self.setWindowTitle('Confirm moving of releases')
        self.setWindowModality(Qt.ApplicationModal)
        self.boxLayout = QVBoxLayout()
        self.releases = releases
        folders = filePaths2FolderPaths(self.releases) 
        for r, f in zip(self.releases, folders):
            r.folder = f
            self.addRelease(r)
        self.buttonConfirm = QPushButton('Confirm')
        self.buttonConfirm.clicked.connect(self.accept)
        self.boxLayout.addWidget(self.buttonConfirm)
        self.buttonReject = QPushButton('Reject')
        self.buttonReject.clicked.connect(self.reject)
        self.boxLayout.addWidget(self.buttonReject)
        self.setLayout(self.boxLayout) 
    
    def confirm(self):
        self.accept()

    def addRelease(self, release):
        groupBox = QGroupBox(release.folder)
        formLayout = QFormLayout(groupBox)
        formLayout.addRow(QLabel('Genre'), QLabel(release.get('genre')[0]))
        formLayout.addRow(QLabel('Year'), QLabel(release.get('year')[0]))
        formLayout.addRow(QLabel('Target Dir'), QLabel(os.path.join(musicBaseDir, '/Riddim Albums/', release.get('genre')[0].upper(), release.get('year')[0])))
        self.boxLayout.addWidget(groupBox)

musicBaseDir = '/home/archm4n/TestMusicBase/'
# moves the containing folders of the selected files to the local 'DataBase' (music folder structure)
def moveFolderToDB():
    print('starting move')
    files = status['selectedfiles']
    print('files: ' + str(files))
    m = MoveFolderDialog(files)
    if m.exec_():
        print('I would suggest to put it in.')

def genFolderArt(hideNotifications=False):
    print "Generating Folder Art..."
    files = status['selectedfiles']
    print('files:' + str(files))
    folders = filePaths2FolderPaths(files)
    print folders
    tmp = 10
    selectedPath = ""
    show_notifications = False
    if len(folders) < 3:
        show_notifications = True
    print "Notifications: "+str(show_notifications)
    for f in folders:
       # filePath = f.get("__dirpath")
        if os.path.isfile(f + "/folder.jpg"):
            if (getDims(f + "/folder.jpg") == (250,250) and show_notifications):
                    msg = QMessageBox()
                    msg.setIcon(QMessageBox.Information)

                    msg.setText(f+ "/folder.jpg with the dimensions (250,250)\nalready exists")
                    msg.setWindowTitle("File already there.")
                    msg.setStandardButtons(QMessageBox.Ok )

                    retval = msg.exec_()
        else:
            d = FolderArtDialog()
            d.setWindowTitle("Choose Image"+f)
            d.setWindowModality(Qt.ApplicationModal)
            imagesExist = d.findImages(f)
            print "imagesExist: "+str(imagesExist) + "\nshow Notifications: "+ str(show_notifications)
            if not imagesExist and not show_notifications:
                continue
            if(d.exec_()):
                selectedPath = d.returnedPath
            if selectedPath != "":
                print('selected: ' + selectedPath)
                folder = os.path.dirname(selectedPath)
                if selectedPath.endswith("/folder.jpg"):
                    print "backing up folder.jpg..."
                    bu_path = os.path.join(folder,'cover.jpg')
                    copyfile(selectedPath, bu_path )
                # TODO: make independent of ffmpeg, use qt functions!
                #smaller_pixmap = pixmap.scaled(32, 32, Qt.KeepAspectRatio, Qt.FastTransformation)
                command = ['ffmpeg','-y','-i',selectedPath,'-vf','scale=250:250', os.path.join(folder , 'folder.jpg')]
                subprocess.Popen(command)

def enableNotifications():
    show_notifications = True
    print "enabled Notifications: "+ str(show_notifications)

def disableNotifications():
    show_notifications = False
    print "disabled Notifications: "+ str(show_notifications)

def init(parent=None):

    def sep():
        k = QAction(parent)
        k.setSeparator(True)
        return k

    action = QAction('Highlight Files without folder.jpg', parent)
    action.setCheckable(True)
    action.connect(action, SIGNAL('toggled(bool)'),
        lambda v: highlight_dupe_field() if v else remove_highlight())

    action_move2musicFolder = QAction('Move to Music folder structure', parent)
    action_move2musicFolder.connect(action_move2musicFolder, SIGNAL('triggered()'),
        moveFolderToDB)
    
    action_genFolderArt = QAction('Generate Folder Art', parent)
    action_genFolderArt.connect(action_genFolderArt, SIGNAL('triggered()'),
        genFolderArt)

    action_toggleNotifications = QAction('Show Notifications', parent)
    action_toggleNotifications.setCheckable(True)
    action_toggleNotifications.connect(action_toggleNotifications, SIGNAL('toggled(bool)'),
        lambda v: enableNotifications() if v else  disableNotifications())

    add_shortcuts('&Plugins', [sep(), action_genFolderArt, action_move2musicFolder, sep(), action, sep(), action_toggleNotifications])
    #add_shortcuts('&Plugins', [sep(), action_genFolderArt])
    #add_shortcuts('&Plugins', [action_toggleNotifications, sep()])

    global obj
    obj.receives = []
    obj.emits = ['highlight']
    connect_control(obj)

